@echo off
mode con: cols=100 lines=30 2> nul
color 1f
where winget.exe >nul 2>nul
IF %ERRORLEVEL% NEQ 0 (
    color 4f
    echo winget chua duoc cai dat tren may tinh cua ban !
    echo Vui long cai dat winget: https://www.microsoft.com/p/app-installer/9nblggh4nns1#activetab=pivot:overviewtab
    start https://www.microsoft.com/p/app-installer/9nblggh4nns1#activetab=pivot:overviewtab
    pause
    exit /b
)
echo winget da duoc cai dat !
goto check
:check
cls
mode con: cols=100 lines=30 2> nul
color 1f
echo Cac phien ban da duoc cai tren may tinh cua ban:
winget list --name "Microsoft Visual"
set /p choose=Ban co muon tiep tuc ? [Y/n]:
if "%choose%"=="y" goto begin
if "%choose%"=="n" goto exit
goto check

:begin
cls
mode con: cols=100 lines=30 2> nul
color 1f
echo Chon phien ban ban muon cai dat:
echo =============
echo -
echo 01. Microsoft Visual C++ 2022 Redistributable (Arm64)
echo 02. Microsoft Visual C++ 2019 Redistributable (Arm64)
echo 03. Microsoft Visual C++ 2017 Redistributable (x86)
echo 04. Microsoft Visual C++ 2017 Redistributable (x64)
echo 05. Microsoft Visual C++ 2015 Redistributable (x86)
echo 06. Microsoft Visual C++ 2015 Redistributable (x64)
echo 07. Microsoft Visual C++ 2015-2022 Redistributable (x86)
echo 08. Microsoft Visual C++ 2015-2022 Redistributable (x64)
echo 09. Microsoft Visual C++ 2015-2019 Redistributable (x86)
echo 10. Microsoft Visual C++ 2015-2019 Redistributable (x64)
echo 11. Microsoft Visual C++ 2013 Redistributable (x86)
echo 12. Microsoft Visual C++ 2013 Redistributable (x64)
echo 13. Microsoft Visual C++ 2012 Redistributable (x86)
echo 14. Microsoft Visual C++ 2012 Redistributable (x64)
echo 15. Microsoft Visual C++ 2010 Redistributable (x86)
echo 16. Microsoft Visual C++ 2010 Redistributable (x64)
echo 17. Microsoft Visual C++ 2008 Redistributable (x86)
echo 18. Microsoft Visual C++ 2008 Redistributable (x64)
echo 19. Microsoft Visual C++ 2005 Redistributable (x86)
echo 20. Microsoft Visual C++ 2005 Redistributable (x64)
echo 00. Exit
echo -
set /p op=Type option:
if "%op%"=="01" goto arm64_2022
if "%op%"=="02" goto arm64_2019
if "%op%"=="03" goto x86_2017
if "%op%"=="04" goto x64_2017
if "%op%"=="05" goto x86_2015
if "%op%"=="06" goto x64_2015
if "%op%"=="07" goto x86_2015_2022
if "%op%"=="08" goto x64_2015_2022
if "%op%"=="09" goto x86_2015_2019
if "%op%"=="1" goto arm64_2022
if "%op%"=="2" goto arm64_2019
if "%op%"=="3" goto x86_2017
if "%op%"=="4" goto x64_2017
if "%op%"=="5" goto x86_2015
if "%op%"=="6" goto x64_2015
if "%op%"=="7" goto x86_2015_2022
if "%op%"=="8" goto x64_2015_2022
if "%op%"=="9" goto x86_2015_2019
if "%op%"=="10" goto x64_2015_2019
if "%op%"=="11" goto x86_2013
if "%op%"=="12" goto x64_2013
if "%op%"=="13" goto x86_2012
if "%op%"=="14" goto x64_2012
if "%op%"=="15" goto x86_2010
if "%op%"=="16" goto x64_2010
if "%op%"=="17" goto x86_2008
if "%op%"=="18" goto x64_2008
if "%op%"=="19" goto x86_2005
if "%op%"=="20" goto x64_2005
if "%op%"=="00" goto exit
if "%op%"=="0" goto exit
echo Vui long chon phien ban muoi cai dat:
goto begin

:arm64_2022
cls
mode con: cols=100 lines=30 2> nul
color 1f
echo Cai dat Microsoft Visual C++ 2022 Redistributable (Arm64)
winget install --id Microsoft.VC++2022Redist-arm64
goto begin

:arm64_2019
cls
mode con: cols=100 lines=30 2> nul
color 1f
echo Caci dat Microsoft Visual C++ 2019 Redistributable (Arm64)
winget install --id Microsoft.VC++2019Redist-arm64
goto begin

:x86_2017
cls
mode con: cols=100 lines=30 2> nul
color 1f
echo Cac dat Microsoft Visual C++ 2017 Redistributable (x86)
winget install --id Microsoft.VC++2017Redist-x86
goto begin

:x64_2017
cls
mode con: cols=100 lines=30 2> nul
color 1f
echo Cai dat Microsoft Visual C++ 2017 Redistributable (x64)
winget install --id Microsoft.VC++2017Redist-x64
goto begin

:x86_2015
cls
mode con: cols=100 lines=30 2> nul
color 1f
echo Cai dat Microsoft Visual C++ 2015 Redistributable (x86)
winget install --id Microsoft.VC++2015Redist-x86
goto begin

:x64_2015
cls
mode con: cols=100 lines=30 2> nul
color 1f
echo Cai dat Microsoft Visual C++ 2015 Redistributable (x64)
winget install --id Microsoft.VC++2015Redist-x64
goto begin

:x86_2015_2022
cls
mode con: cols=100 lines=30 2> nul
color 1f
echo Cai dat Microsoft Visual C++ 2015-2022 Redistributable (x86)
winget install --id Microsoft.VC++2015-2022Redist-x86
goto begin

:x64_2015_2022
cls
mode con: cols=100 lines=30 2> nul
color 1f
echo Cai dat Microsoft Visual C++ 2015-2022 Redistributable (x64)
winget install --id Microsoft.VC++2015-2022Redist-x64
goto begin

:x86_2015_2019
cls
mode con: cols=100 lines=30 2> nul
color 1f
echo Cai dat Microsoft Visual C++ 2015-2019 Redistributable (x86)
winget install --id Microsoft.VC++2015-2019Redist-x86
goto begin

:x64_2015_2019
cls
mode con: cols=100 lines=30 2> nul
color 1f
echo Cai dat Microsoft Visual C++ 2015-2019 Redistributable (x64)
winget install --id Microsoft.VC++2015-2019Redist-x64
goto begin

:x86_2013
cls
mode con: cols=100 lines=30 2> nul
color 1f
echo Cai dat Microsoft Visual C++ 2013 Redistributable (x86)
winget install --id Microsoft.VC++2013Redist-x86
goto begin

:x64_2013
cls
mode con: cols=100 lines=30 2> nul
color 1f
echo Cai dat Microsoft Visual C++ 2013 Redistributable (x64)
winget install --id Microsoft.VC++2013Redist-x64
goto begin

:x86_2012
cls
mode con: cols=100 lines=30 2> nul
color 1f
echo Cai dat Microsoft Visual C++ 2012 Redistributable (x86)
winget install --id Microsoft.VC++2012Redist-x86
goto begin

:x64_2012
cls
mode con: cols=100 lines=30 2> nul
color 1f
echo Cai dat Microsoft Visual C++ 2012 Redistributable (x64)
winget install --id Microsoft.VC++2012Redist-x64
goto begin

:x86_2010
cls
mode con: cols=100 lines=30 2> nul
color 1f
echo Cai dat Microsoft Visual C++ 2010 Redistributable (x86) 
winget install --id Microsoft.VC++2010Redist-x86
goto begin

:x64_2010
cls
mode con: cols=100 lines=30 2> nul
color 1f
echo Cai dat Microsoft Visual C++ 2010 Redistributable (x64)  
winget install --id Microsoft.VC++2010Redist-x64
goto begin

:x86_2008
cls
mode con: cols=100 lines=30 2> nul
color 1f
echo Cai dat Microsoft Visual C++ 2008 Redistributable (x86)
winget install --id Microsoft.VC++2008Redist-x86
goto begin

:x64_2008
cls
mode con: cols=100 lines=30 2> nul
color 1f
echo Cai dat Microsoft Visual C++ 2008 Redistributable (x64)
winget install --id Microsoft.VC++2008Redist-x64
goto begin

:x86_2005
cls
mode con: cols=100 lines=30 2> nul
color 1f
echo Cai dat Microsoft Visual C++ 2005 Redistributable (x86)
winget install --id Microsoft.VC++2005Redist-x86
goto begin

:x64_2005
cls
mode con: cols=100 lines=30 2> nul
color 1f
echo Cai dat Microsoft Visual C++ 2005 Redistributable (x64)
winget install --id Microsoft.VC++2005Redist-x64 
goto begin

:exit
exit /b
